import { AsyncStorage } from 'react-native';
import Config from '../components/Config';

export class NumberPadService {
    constructor() {
    }

    updateTime = (incomingValue, props) => {
        /*
        NOTE: I use the variable PropsForFunc BECAUSE I need access to the parameter
        "props", however I can't pass "props" to other functions as a parameter, so
        I set "props" to a variable, which can then be passed to updateSetsOnly
        */

        let propsForFunc = props;
        if (props.infoForAsync === 'numberOfSets') {
            this.updateSetsOnly(incomingValue, propsForFunc);
        } else {
            this.updateInitialExerciseRestTimes(incomingValue, propsForFunc);
        }
    };

    setInfoToAsync = async () => {
        try {
            const dataForAsync =
                Config.initialTime + ","
            +   Config.exerciseTime + ","
            +   Config.restTime + ","
            +   Config.numberOfSets;
            await AsyncStorage.setItem('presetInfo',dataForAsync, (result, err) => {
                if(err) {
                    console.log('there was a problem setting info', err)
                }
            });
        } catch(e) {
            console.log('did not work this time', e);
        }
        // try {
        //     const dataForAsync =
        //         configService.initialTime + ","
        //     +   configService.exerciseTime + ","
        //     +   configService.restTime + ","
        //     +   configService.numberOfSets;
        //     console.log('this is the configservice version', dataForAsync);
        //     AsyncStorage.setItem('presetInfo', dataForAsync);
        //
        // } catch (e) {
        //     console.log('didnt work this time', e);
        // }

    };

    convertTime = (incoming) => {
        let timeString = incoming;
        if(incoming.length > 2){
            let minutes = timeString.slice(0,-2);
            let seconds = timeString.slice(-2);
            if(parseInt(seconds) > 60) {
                let newSeconds = parseInt(seconds) % 60;
                let newMinutes = parseInt(minutes) + 1;
                newMinutes = newMinutes.toString();
                return { minutes: newMinutes, seconds: newSeconds};
            }
            return {minutes, seconds};

        } else {
            if (parseInt(incoming) > 60){
                let newSeconds = parseInt(incoming) % 60;
                console.log('here is your big ass number');
                return { minutes: '1', seconds: newSeconds.toString()}
            }
            return {minutes: '0', seconds: timeString};
        }
    };

    updateInitialExerciseRestTimes = (incomingValue, props) => {
        if (incomingValue === 'delete') {
            Config[props.infoForAsync] = Config[props.infoForAsync].substring(1);
            this.setInfoToAsync();
            props.showTime({
                incomingTime: Config[props.infoForAsync]
            })
        } else {
            if(Config[props.infoForAsync].length > 4){

            } else {
                Config[props.infoForAsync] = Config[props.infoForAsync] + incomingValue;
                this.setInfoToAsync();
                props.showTime({
                    incomingTime: Config[props.infoForAsync]
                });
            }
        }
    };

    updateSetsOnly = (incomingValue, props) => {
        if (incomingValue === 'delete') {
            Config[props.infoForAsync] = Config[props.infoForAsync].substring(1);
            this.setInfoToAsync();
            props.showTime({
                totalSets: Config[props.infoForAsync]
            });
        } else {
            if(Config[props.infoForAsync].length > 2) {

            } else {
                Config[props.infoForAsync] = Config[props.infoForAsync] + incomingValue;
                console.log('is this working', Config[props.infoForAsync]);
                this.setInfoToAsync();
                props.showTime({
                    totalSets: Config[props.infoForAsync]
                });
            }
        }
    };

    parseTime = (incomingTimeAsString) => {
        if (incomingTimeAsString.length >= 3){
            let minutesOnly = incomingTimeAsString.slice(0,-2);
            let secondsOnly = incomingTimeAsString.slice(-2);
            return {minutesOnly: minutesOnly, secondsOnly: secondsOnly};
        }
        if (incomingTimeAsString.length <= 2){
            return { minutesOnly: '', secondsOnly: incomingTimeAsString };
        }

    };

}

const numberPadService = new NumberPadService();
export { numberPadService };