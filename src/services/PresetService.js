import {
    Preset
} from "./models/Preset";
import {TimerState} from "./models/TimerState";
import {PauseState} from "./models/PauseState";
import {TimeRemaining, TYPE_EXERCISE, TYPE_INITIALCOUNTDOWN, TYPE_REST} from "./models/TimeRemaining";

export class PresetService {

    dateService;

    selectedPreset = new Preset();
    totalSecondsInPhase = 0;
    timerStartTime = null;
    phaseStartTime = null;
    targetTime = null;
    isFinished = null;
    currentTime = null;
    pauseStartTime = null;
    pauseTotalTime = null;
    currentSet = this.selectedPreset.numberOfSets;
    currentPhaseType = TYPE_INITIALCOUNTDOWN;
    remainingSeconds = '';
    updateStateCallbacksArray = [];
    pauseButtonCallbackArray = [];
    resumeStartTime = null;
    isPaused = false;
    initialCountdown = null;
    restTime = null;
    exerciseTime = null;
    numberOfSets = null;
    totalTimeRemaining = null;
    totalTimeStart = null;
    desireTotalTime = null;
    currentTimeTotal = null;
    allSeconds = null;

    constructor(dateService) {
        this.dateService = dateService;
        // this.selectedPreset
        console.log('still inside the constructor looking for initialCountDown', this.initialCountdown)
    }

    currentState() {
        this.updateStateInsidePreset();
        //FOR some reason, totalTimeRemaining comes back in milliseconds
        //So I decided to reset totalTimeRemaining to the total seconds
        if(this.totalTimeRemaining > 3000) {
            this.totalTimeRemaining = '';
        }
        return new TimerState(
            this.currentSet,
            this.isFinished,
            this.totalTimeRemaining,
            this.remainingSeconds,
                new PauseState(this.isPaused, this.resumeStartTime),
                this.selectedPreset,
                new TimeRemaining(this.remainingSeconds, this.currentPhaseType),
            this.currentPhaseType,
            );
    }

    startNotifyingObservers() {
        if(this.phaseStartTime === null) {
            this.currentSet = this.selectedPreset.numberOfSets;
            this.phaseStartTime = this.dateService.now();
            this.totalTimeStart = this.dateService.now();
        }
        setInterval(() => {
            if(this.isPaused === true) {
                console.log('now we are paused');
            } else {
                this.updateStateCallbacksArray.forEach
                ((updateStateCallback) => updateStateCallback(this.currentState()));
            }
            
        }, 1000);
    }

    // notifySubjectOfPause = () => {
    //     // console.log('iinside the notifysubject of pause')
    //     this.pauseButtonCallbackArray.forEach((pauseCallback) => {
    //         pauseCallback(this.pause());
    //     });
    // };

    calculateTotalTimeRemaining = () => {
        this.currentTimeTotal = this.dateService.now();
        this.allSeconds = ( (this.selectedPreset.initialCountDown)
                    + ((this.selectedPreset.restTime + this.selectedPreset.exerciseTime)
                    * this.selectedPreset.numberOfSets ));

        let tempNum = this.allSeconds * 1000;
        this.desireTotalTime = this.totalTimeStart + tempNum;
        this.desireTotalTime = this.desireTotalTime - this.currentTime;
        //the code below is if you want to remove the last rest period off of totalTimeRemaining
        // this.totalTimeRemaining = (Math.round(this.desireTotalTime/1000) - this.selectedPreset.restTime);
        this.totalTimeRemaining = (Math.round(this.desireTotalTime/1000) );

        if (this.totalTimeRemaining < 30000 ) {
            return this.totalTimeRemaining;
        } else {
            // console.log('else block located', this.totalTimeRemaining);
            return tempNum;
        }
    };

    calculateTotalTimeInPhase = (phaseTYpe) => {
        this.currentTime = this.dateService.now();
        this.totalSecondsInPhase = phaseTYpe * 1000;

        if(this.resumeStartTime === null){

            this.targetTime = this.phaseStartTime + this.totalSecondsInPhase;
            this.remainingSeconds = this.targetTime - this.currentTime;
            this.remainingSeconds = Math.round(this.remainingSeconds/1000);
            if (this.remainingSeconds === 0 ){
                return this.nextPhase(this.currentPhaseType);
            }
            return this.remainingSeconds;

        } else {
            this.targetTime = (this.phaseStartTime + this.pauseTotalTime)+ this.totalSecondsInPhase;
            this.remainingSeconds = this.targetTime - this.currentTime;
            this.remainingSeconds = Math.round(this.remainingSeconds/1000);
            console.log('calculating time remaining inside pause', this.pauseTotalTime);
            console.log('heres your resume start time', this.resumeStartTime);
            
            return this.remainingSeconds
        }
    };
    
    updateStateInsidePreset = () => {
        this.calculateTotalTimeRemaining();

        let phaseType = this.getTotalTimeRemainingBasedOnType();
        let nextPhaseCheck = this.calculateTotalTimeInPhase(phaseType);
        if (nextPhaseCheck.type === 'exercise'){
            console.log('here is what you need', nextPhaseCheck);
            this.phaseStartTime = this.dateService.now();
            this.currentPhaseType = nextPhaseCheck.type;
            let phaseType = this.getTotalTimeRemainingBasedOnType();
            this.calculateTotalTimeInPhase(phaseType);

        }

        if (nextPhaseCheck.type === 'rest'){
            this.phaseStartTime = this.dateService.now();
            this.currentPhaseType = nextPhaseCheck.type;
            console.log('inside the rest phase of next phase check', this.currentSet);
            let currentSet = this.updateCurrentSet(this.currentPhaseType, this.currentSet);
            if (currentSet < 1) {
                console.log('this is where everything stops');
                this.isFinished = true;
                this.currentSet = 0;
                this.totalTimeRemaining = null;
                this.reset();

            } else {
                this.currentSet = currentSet;
                let phaseType = this.getTotalTimeRemainingBasedOnType();
                this.calculateTotalTimeInPhase(phaseType);
            }
        }
    };

    

    nextPhase = (currentTimerType) => {
        if (TYPE_INITIALCOUNTDOWN === currentTimerType) {
            return new TimeRemaining(this.selectedPreset.exerciseTime, TYPE_EXERCISE);
        }
        if (TYPE_REST === currentTimerType) {
            return new TimeRemaining(this.selectedPreset.exerciseTime, TYPE_EXERCISE);
        }
        if (TYPE_EXERCISE === currentTimerType) {
            return new TimeRemaining(this.selectedPreset.restTime, TYPE_REST);
        }
    }

    isCurrentlyPaused = () => {
        console.log('inside isCurrentlyPaused');
        return this.pauseStartTime != null;
    }

    // pauseButtonObserver = (pauseButtonCallback) => {
    //     this.pauseButtonCallbackArray.push(pauseButtonCallback);
    // };

    pause = () => {
        this.isPaused = true;
        this.pauseStartTime = this.dateService.now();
    }
    resume = () => {
        this.isPaused = false;
        this.pauseStartTime = null;
    };

    getTotalTimeRemainingBasedOnType = () => {
        if (TYPE_INITIALCOUNTDOWN === this.currentPhaseType) {
            return this.selectedPreset.initialCountDown;
        } else if (TYPE_EXERCISE === this.currentPhaseType) {
            return this.selectedPreset.exerciseTime;
        } else if (TYPE_REST === this.currentPhaseType) {
            return this.selectedPreset.restTime;
        }
    };


    updateCurrentSet = (currentTimerType, currentSet) => {
        console.log('inside upddate current set',currentTimerType, currentSet);
        if (TYPE_REST === currentTimerType) {
            return currentSet - 1;
        }
        return currentSet;
    };

    
    addObserver = (updateStateCallback) => {
        this.updateStateCallbacksArray.push(updateStateCallback);
    };

    reset = () => {
        this.updateStateCallbacksArray.splice(0);
        this.phaseStartTime = null;
        this.totalSecondsInPhase = 0;
        this.timerStartTime = null;
        this.phaseStartTime = null;
        this.targetTime = null;
        this.currentTime = null;
        this.pauseStartTime = null;
        this.pauseTotalTime = null;
        this.totalTimeRemaining = null;
        this.remainingSeconds = null;
        return null;

    }
}