export class ConfigService {
    constructor(exerciseTime, initialTime, restTime, numberOfSets){
        this.exerciseTime = exerciseTime;
        this.initialTime = initialTime;
        this.restTime = restTime;
        this.numberOfSets = numberOfSets
    }
    // const totalSet = numberOfSets;
}
const configService = new ConfigService();
export { configService };