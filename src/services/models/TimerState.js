export class TimerState {
    set;
    isFinished;
    // activeTimer;
    totalTimeRemaining;
    timeRemaining; // TimeRemaining class
    pausedState;
    preset;
    newTotalTime;
    currentPhase;

    constructor(set, isFinished,
                totalTimeRemaining, timeRemaining, pausedState, preset, newTotalTime
                , currentPhase
    ) {
        this.set = set;
        this.isFinished = isFinished;
        // this.activeTimer = activeTimer;
        this.totalTimeRemaining = totalTimeRemaining;
        this.timeRemaining = timeRemaining;
        this.pausedState = pausedState;
        this.preset = preset;
        this.newTotalTime = newTotalTime;
        this.currentPhase = currentPhase;


    }
}

