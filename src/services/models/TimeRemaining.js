export const TYPE_EXERCISE = 'exercise';
export const TYPE_REST = 'rest';
export const TYPE_INITIALCOUNTDOWN = 'initialCountdown';

export class TimeRemaining {
    
    seconds;
    type;

    constructor(seconds, type) {
        this.seconds = seconds;
        this.type = type;
        
    }
}