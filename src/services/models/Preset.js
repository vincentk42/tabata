export class Preset {
 
    initialCountDown;
    exerciseTime;
    restTime;
    numberOfSets;


     
    constructor(initialCountDown, exerciseTime, restTime, numberOfSets) {
        this.initialCountDown = initialCountDown;
        this.exerciseTime = exerciseTime;
        this.restTime = restTime;
        this.numberOfSets = numberOfSets;
    }
}
    