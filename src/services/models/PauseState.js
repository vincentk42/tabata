export class PauseState {
    paused;
    timeAtPause;

    constructor(paused, timeAtPause) {
        this.paused = paused;
        this.timeAtPause = timeAtPause;
    }
}