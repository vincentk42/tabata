import {PresetService} from "./PresetService";
import {DateService} from "./DateService";

export class RootService {
    dateService = new DateService();
    presetService = new PresetService(this.dateService);

    constructor() {
        // this.presetService.startNotifyingObservers();
        // this.presetService.notifySubjectToGetTotalSeconds();
        // this.presetService.notifySubjectOfCountdown();
    }
}