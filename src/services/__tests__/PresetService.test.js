import {PresetService} from "../PresetService";
import {Preset} from "../models/Preset";
import {TYPE_INITIALCOUNTDOWN} from "../models/TimeRemaining";

describe("PresetService", function() {

    let dateService;
    let service;
    beforeEach(function () {
        dateService = {now: () => {}};
        service = new PresetService(dateService);
    });

    it("should set initial variables when start is called", function() {
        // 1) initialization
        service.selectedPreset = new Preset(null, null, null, 800);
        spyOn(dateService, 'now').and.returnValue("some specific time");

        // 2) call method under test
        service.start();

        // 3) assertions
        expect(service.pauseTotalTime).toEqual(0);
        expect(service.currentPhaseType).toEqual(TYPE_INITIALCOUNTDOWN);
        expect(service.currentSet).toEqual(800);
        expect(dateService.now.calls.count()).toEqual(1);
        expect(service.timerStartTime).toEqual("some specific time");
        expect(service.phaseStartTime).toEqual("some specific time");

    });

    it("should calculate total time remaining in current phase", function () {
        service.phaseStartTime = 500;
        service.pauseTotalTime = 100;
        service.currentPhaseType = TYPE_INITIALCOUNTDOWN;
        service.selectedPreset = new Preset(200, null, null, null);

        const result = service.calculateTotalTimeInPhase(650);

        expect(result).toEqual(150);
    });

    it("should switch to the next phase when current phase is over", function () {
        //1. initialization

        //2. call method under test

        //3. assertions

    });

    it("should not switch to next phase if user has hit the pause button", function () {
        //initialization
        service.selectedPreset = new Preset(8, null, null, null);

        const startTime = 0;
        const pauseStartTime = 1;
        const updateTime = 1000;
        spyOn(dateService, 'now').and.returnValue(startTime, pauseStartTime, updateTime);
        // start timer
        service.start();
        // pause
        service.pause();
        // wait till phase should be over

        //method call
        service.updateState();

        //assertions
        // check that we have not changed phase (totalTimeRemaining hasn't decreased)
        expect(service.currentPhaseType).toEqual(TYPE_INITIALCOUNTDOWN);
        expect(service.isCurrentlyPaused()).toBe(true);
        expect(service.calculateTotalTimeInPhase(updateTime)).toEqual(7);

    });
});

