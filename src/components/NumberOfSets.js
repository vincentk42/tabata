import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Header from './common/Header';
import NumberPad from './common/NumberPad';
import Config from './Config';

class NumberOfSets extends Component {
    constructor() {
        super();
        this.state = {
            totalSets: '',

        };
        this.infoForAsync = 'numberOfSets';
    }

    componentDidMount = () => {

       this.setState({
           totalSets: Config.numberOfSets
       });

    };

    showTime = (props) => {
        // console.log('here is the value coming thru', props);
        this.setState({
            totalSets: props.totalSets
        });
    };
    render() {
        return(
            <View style={styles.mainView}>
                <Header headerText="Number of Sets"/>
                <View style={styles.readoutView}>
                    <Text style={styles.textColorOnly}>sets: {this.state.totalSets}</Text>
                </View>
                <NumberPad
                    showTime={this.showTime}
                    infoForAsync={this.infoForAsync}
                />
            </View>
        )
    }
}

const styles = {
    mainView: {
        flex: 1,
    },
    readoutView: {
        flex: 1,
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textColorOnly: {
        color: 'white',
        fontSize: 25,
    },
};
export default NumberOfSets;