import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Header from './common/Header';
import NumberPad from './common/NumberPad';
import {numberPadService} from "../services/NumberPadService";
import Config from "./Config";

class RestTime extends Component {
    constructor() {
        super();
        this.state = {
            minutesOnly: '',
            secondsOnly: '',
        };
        this.infoForAsync = 'restTime';
    }

    componentWillMount = () => {
        let newTime = Config.restTime;
        let timeObj = numberPadService.parseTime(newTime);
        this.setState({
            minutesOnly: timeObj.minutesOnly,
            secondsOnly: timeObj.secondsOnly,
        })
    };

    showTime = (props) => {
        let timeObj = numberPadService.parseTime(props.incomingTime);
        this.setState({
            minutesOnly: timeObj.minutesOnly,
            secondsOnly: timeObj.secondsOnly,
        });
    };
    render() {
        return(
            <View style={styles.mainView}>
                <Header headerText="Rest Time"/>
                <View style={styles.readoutView}>
                    <Text style={styles.textColorOnly}>minutes: {this.state.minutesOnly}</Text>
                    <Text style={styles.textColorOnly}> seconds: {this.state.secondsOnly}</Text>
                </View>
                <NumberPad
                    showTime={this.showTime}
                    infoForAsync={this.infoForAsync}
                />
            </View>
        )
    }
}

const styles = {
    mainView: {
        flex: 1,
    },
    readoutView: {
        flex: 1,
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textColorOnly: {
        color: 'white',
        fontSize: 25,
    },
};

export default RestTime;