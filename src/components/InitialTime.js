import React, { Component } from 'react';
import { Text, View } from 'react-native';
import Header from './common/Header';
import NumberPad from './common/NumberPad';
import Config from './Config';
import { numberPadService } from "../services/NumberPadService";


class InitialTime extends Component {
    constructor(props) {
        super(props);
        console.log('this here', props);
        this.state = {
            minutesOnly: '',
            secondsOnly: '',
        };
        this.infoForAsync = 'initialTime';
    }

    componentWillMount = () => {
        let newTime = Config.initialTime;
        let timeObj = numberPadService.parseTime(newTime);
        this.setState({
            minutesOnly: timeObj.minutesOnly,
            secondsOnly: timeObj.secondsOnly,
        })
    };

    showTime = (props) => {
        console.log('here is the value coming thru', props);
        let timeObj = numberPadService.parseTime(props.incomingTime);
        console.log('heres your timeobj', timeObj);
        this.setState({
            minutesOnly: timeObj.minutesOnly,
            secondsOnly: timeObj.secondsOnly,
        })
    };
    render() {
        return(
            <View style={styles.mainView}>
                <Header headerText="Initial Time"/>
                <View style={styles.readoutView}>
                    <Text style={styles.textColorOnly}>minutes: {this.state.minutesOnly}</Text>
                    <Text style={styles.textColorOnly}> seconds: {this.state.secondsOnly}</Text>
                </View>
                <NumberPad
                    showTime={this.showTime}

                    infoForAsync={this.infoForAsync}
                />
            </View>
        )
    }
}

const styles = {
    mainView: {
        flex: 1,
    },
    readoutView: {
        flex: 1,
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textColorOnly: {
        color: 'white',
        fontSize: 25,
    },
};
export default InitialTime;