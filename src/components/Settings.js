import React, {Component} from 'react';
import {Text, View, TouchableOpacity, AsyncStorage } from 'react-native';
import {configService, ConfigService} from "../services/ConfigService";
import Config from './Config';
import {numberPadService} from "../services/NumberPadService";

class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            initialTime: '',
            exerciseTime: '',
            restTime: '',
            numberOfSets: '',
            initialTimeBoth: {
                minutes: '',
                seconds: '',
            },
            exerciseTimeBoth: {
                minutes: '',
                seconds: '',
            },
            restTimeBoth: {
                minutes: '',
                seconds: '',
            }

        };

    }

    componentDidMount = async () => {
        try {
            await AsyncStorage.getItem('presetInfo', (err, result) => {
                if(err){
                    console.log('could not retrieve info', err)
                } else {
                    if(result) {
                        const arrayOfPresets = result.split(",");
                        Config.initialTime = arrayOfPresets[0];
                        Config.exerciseTime = arrayOfPresets[1];
                        Config.restTime = arrayOfPresets[2];
                        Config.numberOfSets = arrayOfPresets[3];

                        this.setState({
                            initialTime: Config.initialTime,
                            exerciseTime: Config.exerciseTime,
                            restTime: Config.restTime,
                            numberOfSets: Config.numberOfSets,
                            initialTimeBoth: {
                                minutes: this.convertTime(Config.initialTime).minutes,
                                seconds: this.convertTime(Config.initialTime).seconds,
                            },
                            exerciseTimeBoth: {
                                minutes: this.convertTime(Config.exerciseTime).minutes,
                                seconds: this.convertTime(Config.exerciseTime).seconds,
                            },
                            restTimeBoth: {
                                minutes: this.convertTime(Config.restTime).minutes,
                                seconds: this.convertTime(Config.restTime).seconds,
                            },

                        });
                    }
                }
            });
        } catch (e) {
            console.log('failed to get info');
        }
        /*
        NOTE: The code below is using the addlistener to refresh the page when
        a user hits the back button.
        */
        this.props.navigation.addListener(
            'willFocus', () => {
                this.setState({
                    initialTimeBoth: {
                        minutes: this.convertTime(Config.initialTime).minutes,
                        seconds: this.convertTime(Config.initialTime).seconds,
                    },
                    exerciseTimeBoth: {
                        minutes: this.convertTime(Config.exerciseTime).minutes,
                        seconds: this.convertTime(Config.exerciseTime).seconds,
                    },
                    restTimeBoth: {
                        minutes: this.convertTime(Config.restTime).minutes,
                        seconds: this.convertTime(Config.restTime).seconds,
                    },
                    numberOfSets: Config.numberOfSets,
                })
            }
        )
    };

    convertTime = (incomingTime) => {
        return numberPadService.convertTime(incomingTime);
    };

    goToNumPad = (value) => {
        this.props.navigation.navigate(value);
    };

    render() {
        return(
            <View style={styles.mainView}>
                <View style={styles.measuresView}>
                    <View style={styles.measuresHeadingView}>
                        <Text style={styles.measuresText}>Measures</Text>
                    </View>

                    <TouchableOpacity
                        onPress={() => this.goToNumPad('InitialTime')}
                        style={styles.initialTimeView}
                    >
                        <View style={styles.initialTimeTextView}>
                            <View style={styles.logoView}>
                                <Text style={{
                                    fontFamily: 'FontAwesome',
                                    fontSize: 25,
                                    color: 'yellow',
                                }}>
                                
                                </Text>
                            </View>

                            <View style={styles.typeView}>
                                <Text style={styles.typeOfTimeText}>
                                    initial time
                                </Text>
                            </View>

                            <View style={styles.valueView}>
                                <Text style={styles.valueText}>
                                    {this.state.initialTimeBoth.minutes} m
                                    {this.state.initialTimeBoth.seconds} s
                                </Text>

                            </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.goToNumPad('ExerciseTime')}
                        style={styles.exerciseTimeView}>
                        <View style={styles.exerciseTimeTextView}>
                            <View style={styles.logoView}>
                                <Text style={{
                                    fontFamily: 'FontAwesome',
                                    fontSize: 25,
                                    color: 'green',
                                }}>
                                    
                                </Text>
                            </View>

                            <View style={styles.typeView}>
                                <Text style={styles.typeOfTimeText}>
                                    exercise time
                                </Text>
                            </View>

                            <View style={styles.valueView}>
                                <Text style={styles.valueText}>
                                    {this.state.exerciseTimeBoth.minutes} m
                                    {this.state.exerciseTimeBoth.seconds} s
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.goToNumPad('RestTime')}
                        style={styles.restTimeView}>
                        <View style={styles.restTimeTextView}>
                            <View style={styles.logoView}>
                                <Text style={{
                                    fontFamily: 'FontAwesome',
                                    fontSize: 25,
                                    color: 'red',
                                }}>
                                    
                                </Text>
                            </View>

                            <View style={styles.typeView}>
                                <Text style={styles.typeOfTimeText}>
                                    rest time
                                </Text>
                            </View>

                            <View style={styles.valueView}>
                                <Text style={styles.valueText}>
                                    {this.state.restTimeBoth.minutes} m
                                    {this.state.restTimeBoth.seconds} s
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.goToNumPad('NumberOfSets')}
                        style={styles.numberOfSetsView}>
                        <View style={styles.numberOfSetsTextView}>
                            <View style={styles.logoView}>
                                <Text style={{
                                    fontFamily: 'FontAwesome',
                                    fontSize: 25,
                                    color: 'white',
                                }}>
                                    
                                </Text>
                            </View>

                            <View style={styles.typeView}>
                                <Text style={styles.typeOfTimeText}>
                                    number of sets
                                </Text>
                            </View>

                            <View style={styles.valueView}>
                                <Text style={styles.valueText}>
                                    {this.state.numberOfSets}
                                </Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.presetsView}>
                    <View style={styles.presetTextView}>
                        <Text style={styles.presetTextOnly}>Presets</Text>
                    </View>
                </View>
            </View>
        );
    }
}

export default Settings;

const styles = {
    mainView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent:'center',
        alignItems: 'center',
    },

    measuresView: {
        flex: 1,
        backgroundColor: 'blue',
        // justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100%',
        height: '15%',
    },

    measuresHeadingView: {
        backgroundColor: 'black',
        justifyContent:'center',
        alignItems: 'center',
        width: '100%',
        flex: 1,
    },
    measuresText: {
        fontSize: 25,
        color: 'white',
        fontWeight: 'bold',
    },
    fontLogoStyleInitialTime: {
        fontFamily: 'FontAwesome',
        fontSize: 25,
        color: 'white',
    },
    valueText: {
        color: 'white',
        fontSize: 20,
    },
    typeOfTimeText: {
        color: 'white',
        fontSize: 20,
    },
    initialTimeView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        borderBottomColor: 'white',
        borderWidth: 1,
    },
    logoView: {
        justifyContent: 'center',
        flex: 1,
        alignItems: 'flex-end',

    },
    typeView: {
        paddingLeft: 10,
        justifyContent: 'center',
        flex: 3,
        alignItems: 'flex-start',
    },
    valueView: {
        justifyContent: 'center',
        flex: 5,
        alignItems: 'flex-start',
    },
    initialTimeTextView: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',

    },
    exerciseTimeView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        borderBottomColor: 'white',
        borderWidth: 1,

    },
    exerciseTimeTextView: {
        flexDirection: 'row',
    },
    restTimeView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        borderBottomColor: 'white',
        borderWidth: 1,
    },
    restTimeTextView: {
        flexDirection: 'row',

    },
    numberOfSetsView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row',
        width: '100%',
        borderBottomColor: 'white',
        borderWidth: 1,

    },
    numberOfSetsTextView: {
        flexDirection: 'row',

    },
    presetsView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent:'center',
        alignItems: 'center',
    },
    presetTextView:{
        flex: 1,
        backgroundColor: 'black',
        justifyContent:'center',
        alignItems: 'flex-start',
    },
    presetTextOnly: {
        fontSize: 25,
        color: 'white'
    },

};