import React from 'react';
import { Text, View } from 'react-native';

const Header = (props) => {
    return (
        <View style={styles.mainHeaderView}>
            <Text style={styles.headerTextOnly}>{props.headerText}</Text>
        </View>
    )
};

const styles = {
    mainHeaderView: {
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60,
        paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 10 },
        shadowOpacity: 0.8,
        position: 'relative'
    },
    headerTextOnly: {
        fontSize: 25,
        color: 'white',
    },
};

export default Header;