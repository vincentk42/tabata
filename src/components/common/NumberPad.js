import React, { Component } from 'react';
import { Text, View, TouchableOpacity,  } from 'react-native';
import { numberPadService } from "../../services/NumberPadService";


class NumberPad extends Component {
    constructor() {
        super();
    }

    updateTime = (incomingValue) => {
        numberPadService.updateTime(incomingValue, this.props);
    };

    render() {
        return (
            <View style={styles.mainView}>
                <View style={styles.firstRowView}>
                    <TouchableOpacity
                        onPress={() => this.updateTime('1')}
                        style={styles.oneView}>
                        <Text style={styles.numberTextOnly}>1</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.updateTime('2')}
                        style={styles.twoView}>
                        <Text style={styles.numberTextOnly}>2</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.updateTime('3')}
                        style={styles.threeView}>
                        <Text style={styles.numberTextOnly}>3</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.secondRowView}>
                    <TouchableOpacity
                        onPress={() => this.updateTime('4')}
                        style={styles.fourView}>
                        <Text style={styles.numberTextOnly}>4</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.updateTime('5')}
                        style={styles.fiveView}>
                        <Text style={styles.numberTextOnly}>5</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.updateTime('6')}
                        style={styles.sixView}>
                        <Text style={styles.numberTextOnly}>6</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.thirdRowView}>
                    <TouchableOpacity
                        onPress={() => this.updateTime('7')}
                        style={styles.sevenView}>
                        <Text style={styles.numberTextOnly}>7</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.updateTime('8')}
                        style={styles.eightView}>
                        <Text style={styles.numberTextOnly}>8</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.updateTime('9')}
                        style={styles.nineView}>
                        <Text style={styles.numberTextOnly}>9</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.fourthRowView}>
                    <View style={styles.noView}>
                        <Text></Text>
                    </View>

                    <TouchableOpacity
                        onPress={() => this.updateTime('0')}
                        style={styles.zeroView}>
                        <Text style={styles.numberTextOnly}>0</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() => this.updateTime('delete')}
                        style={styles.deleteView}>
                        <Text style={styles.numberTextOnly}>x</Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

const styles = {
    mainView: {
        flex: 1,
        backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    firstRowView: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'blue',
    },
    oneView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    twoView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    threeView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    secondRowView: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'black',

    },
    fourView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    fiveView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sixView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    thirdRowView: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'black',
    },
    sevenView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    eightView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    nineView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    fourthRowView: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: 'black',
    },
    noView: {
        flex: 1,
        backgroundColor: 'black'
    },
    zeroView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    deleteView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    numberTextOnly: {
        fontSize: 25,
        color: 'white',
    }
};

export default NumberPad;