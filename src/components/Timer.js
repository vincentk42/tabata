import React, {Component} from 'react';
import {Text, TouchableOpacity, View, Alert, AsyncStorage} from 'react-native';
import {TYPE_REST, TYPE_EXERCISE} from '../services/models/TimeRemaining';
import {RootService} from "../services/RootService";
import { Preset} from "../services/models/Preset";

class Timer extends Component {
    constructor() {
        super();
        this.state = {
            defaultMode: true,
            playButtonState: false,
            remainingTime: '',
            isPaused: false,
            timeOfPause: '',
            phaseType: '',
            currentSet: '',
            isFinished: false,
            isFinishedMessage: 'FINISHED!',
            totalTimeRemaining: null,
            currentPhase: null,
        };
        this.paused = false;

    }

    componentDidMount = async () =>{
        console.log('before the try block inside cdm');
      try{
          await AsyncStorage.getItem('presetInfo', (err, result) => {
             if(err) {
                 console.log('there was a problem retrieving data', err);
             } else {
                 if(result) {
                     const dataForPresetModel = result.split(",");
         console.log('here is your array', this.props.screenProps.presetService);

             this.props.screenProps.presetService.selectedPreset.initialCountDown = parseInt(dataForPresetModel[0]);
             this.props.screenProps.presetService.selectedPreset.exerciseTime = parseInt(dataForPresetModel[1]);
             this.props.screenProps.presetService.selectedPreset.restTime = parseInt(dataForPresetModel[2]);
             this.props.screenProps.presetService.selectedPreset.numberOfSets = parseInt(dataForPresetModel[3]);

                     console.log('after setting preset data',
         this.props);

                 } else {
                     console.log('could not set preset Data');
                 }
             }
          });
      } catch (e) {
          console.log('failed to load data', e);
      }

    };

    playButtonPressed = () => {
        this.props.screenProps.presetService.addObserver(this.updateStateTimerJS);
        this.props.screenProps.presetService.startNotifyingObservers();

        this.setState({
            isPaused: false,
            defaultMode: false,
            playButtonState: true,
        });
    };
    pauseButtonPressed = () => {
            this.isPaused = !this.isPaused;
            if(this.isPaused === true) {
                this.props.screenProps.presetService.pause();
            } if(this.isPaused === false) {
                this.props.screenProps.presetService.resume();
            }

    };

    // pauseButtonAlert = (isPaused) => {
    //     this.setState({
    //         isPaused: isPaused
    //     })

    // };

    updateStateTimerJS = (newState) => {
        console.log('heres your new state', newState);
        if(newState.pausedState.paused === true){
            remainingTime: newState.pausedState.timeAtPause
        } else {
            this.setState({
                remainingTime: newState.newTotalTime.seconds,
                phaseType: newState.newTotalTime.type,
                currentSet: newState.set,
                isFinished: newState.isFinished,
                totalTimeRemaining: newState.totalTimeRemaining,
                currentPhase: newState.currentPhase,
            })
        }


    };

    goToSettingsPage = () => {
        this.props.navigation.navigate('Settings')
    };


    getBottomMenu = () => {
        return (
            <View style={styles.bottomBoxView}>
                <View style={styles.playButtonView}>
                    <TouchableOpacity
                        onPress={this.playButtonPressed}
                    >
                        <Text style={styles.playButtonText}></Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.pauseButtonView}>
                    <TouchableOpacity
                        onPress={this.pauseButtonPressed}
                    >
                        <Text style={styles.pauseButtonText}></Text>
                    </TouchableOpacity>

                </View>
                <View style={styles.settingsView}>
                    <TouchableOpacity
                        onPress={
                            this.goToSettingsPage
                        }
                    >
                        <Text style={styles.settingsText}></Text>
                    </TouchableOpacity>
                </View>


            </View>
        );

    };
    render() {
        const state = this.state;
        if (state == null) {
            return null;
        }
        return (
            <View style={styles.mainView}>
                <View style={styles.middleBoxView}>
                {this.state.defaultMode ? this.getDefaultMode() : this.getPlayMode()}
                </View>
                {this.getBottomMenu()}
            </View>
        )
    }

    getDefaultMode =() => {
        return <View style={styles.defaultModeView}>
            <Text style={styles.numberTextMain}>Total Time Remaining:
                {this.state.isPaused ? this.state.timeOfPause : this.state.remainingTime}</Text>
            <Text style={{color: 'green'}}>Default mode</Text>
            <View>

            </View>
        </View>;
    };

    // getFinishedMode = () => {
    //   return(
    //     <View style={styles.defaultModeView}>
    //       <View style={styles.playModeTopView}>
    //           <View style={styles.playModeTopTextView}>
    //               <Text style={{color: 'white'}}>Finished!</Text>
    //           </View>
    //       </View>
    //     </View>);
    // };

    getPlayMode = ()=> {
        return <View style={styles.playModeView}>
            <View style={styles.playModeTopView}>
                <View style={styles.playModeTopTextView}>
                    <Text style={{color: 'white'}}>{this.state.phaseType}</Text>
                </View>
            </View>

            <View style={styles.playModeMiddleView}>
                <View style={styles.playModeMiddleTextView}>
                    <Text style={{color: 'white', fontSize: 40}}>{this.state.isFinished ? this.state.isFinishedMessage : this.state.remainingTime }</Text>
                </View>
            </View>

            <View style={styles.playModeBottomView}>
                <View style={styles.playModeBottomUpperView}>
                    <View style={styles.setsView}>
                        <Text style={{color: 'white'}}>sets</Text>
                    </View>

                    <View style={styles.timeView}>
                        <Text style={{color: 'white'}}>{this.state.currentPhase}</Text>
                    </View>

                    <View style={styles.totalTimeView}>
                        <Text style={{color: 'white'}}>total time remaining</Text>
                    </View>
                </View>

                <View style={styles.playModeBottomLowerView}>
                    <View style={styles.lowerSetsView}>
                        <Text style={{color: 'white', fontSize: 25}}>{this.state.currentSet}</Text>
                    </View>

                    <View style={styles.regTimeView}>
                        <Text style={{color: 'white', fontSize: 25}}>{this.state.remainingTime}</Text>
                    </View>
                    <View style={styles.lowerTimeRemainingView}>
                        <Text style={{color: 'white', fontSize: 25}}>
                            {this.state.totalTimeRemaining}</Text>
                    </View>
                </View>
            </View>
        </View>;
    };


}

export default Timer;

const styles = {
    mainView: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    defaultModeView: {
        flex: 1,
        height: '100%',
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    playModeView: {
        height: '100%',
        width: '100%',
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },

    playModeTopView: {
        flex: 2,
        height: '100%',
        width: '100%',
        backgroundColor: 'black',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    playModeTopTextView: {
        width: '30%',
        height: '50%',
        // backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center',
    },
    playModeMiddleView: {
        flex: 3,
        height: '100%',
        width: '100%',
        backgroundColor: 'black',
        justifyContent: 'center',
    },
    playModeMiddleTextView: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    playModeBottomView: {
        flex: 3,
        height: '100%',
        width: '100%',
        backgroundColor: 'green',
        flexDirection: 'column'

    },
    playModeBottomUpperView: {
        flex: 1,
        // backgroundColor: 'yellow',
        flexDirection: 'row',
    },
    setsView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    timeView: {
        flex: 2,
        backgroundColor: 'black',
        alignItems: 'center',
        justifyContent: 'flex-end',


    },
    totalTimeView: {
        flex: 3,
        backgroundColor: 'black',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

    },
    playModeBottomLowerView: {
        flex: 1,
        backgroundColor: 'blue',
        flexDirection: 'row'
    },

    lowerSetsView: {
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
    },
    regTimeView: {
        flex: 2,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'center'
    },
    lowerTimeRemainingView: {
        flex: 3,
        backgroundColor: 'black',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    middleBoxView: {
        flex: 5,

        backgroundColor: 'black',
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    alternateTextView: {
        backgroundColor: 'white',

    },
    numberText: {
        color: 'red',
        fontSize: 20,
    },
    numberTextMain: {
        color: 'white',
        fontSize: 50,
    },
    bottomBoxView: {
        flex: 1,
        backgroundColor: '#212121',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        // backgroundColor: 'white',
        flexDirection: 'row'
    },
    playButtonView: {
        flex: 1,
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'green',

    },
    pauseButtonView: {
        flex: 1,
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'green',

    },
    pauseButtonText: {
        fontFamily: 'FontAwesome',
        fontSize: 25,
        color: 'white',
    },
    settingsView: {
        flex: 1,
        // backgroundColor: 'yellow',
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    playButtonText: {
        fontFamily: 'FontAwesome',
        fontSize: 25,
        color: 'white',
    },

    settingsText: {
        fontFamily: 'FontAwesome',
        fontSize: 25,
        color: 'white',
    },
    mainModalView: {
        backgroundColor: 'white',
        flex: 1

    },
};
