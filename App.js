import React, {
    Component
} from 'react';
import {
    StackNavigator,
} from 'react-navigation';
import Timer from './src/components/Timer';
import {PresetService} from './src/services/PresetService';
import Settings from "./src/components/Settings";
import InitialTime from './src/components/InitialTime';
import {RootService} from "./src/services/RootService";
import ExerciseTime from "./src/components/ExerciseTime";
import RestTime from "./src/components/RestTime";
import NumberOfSets from "./src/components/NumberOfSets";


const Navigation = StackNavigator({
        Timer: {
            screen: Timer
        },
        Settings: {
            screen: Settings
        },
        InitialTime: {
            screen: InitialTime
        },
        ExerciseTime: {
            screen: ExerciseTime
        },
        RestTime: {
            screen: RestTime
        },
        NumberOfSets: {
            screen: NumberOfSets
        }
    }, {
        initialRouteName: 'Timer',
        headerMode: 'none',
    }
)
export default class App extends Component {
    render() {
        return (
            <Navigation screenProps={new RootService()}>
            </Navigation>

        );
    }
}